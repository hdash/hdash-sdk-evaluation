import { Device, DeviceDescriptor, ProtocolType } from '@hdash/device';
import { GenericGlucometerDriver } from '@hdash/driver-ble';

class MyDeviceDescriptor extends DeviceDescriptor {

    constructor() {
        super("Meu dispositivo XPTO",
            [
                {
                    connectivityProtocol: ProtocolType.Ble,
                    communicationProtocol: GenericGlucometerDriver,
                    filter: (device: Device) => device.uri.includes("meter"),
                }
            ]);
    }

}

export const DEVICE_DESCRIPTOR = new MyDeviceDescriptor();