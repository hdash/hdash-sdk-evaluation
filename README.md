# HDash SDK Evaluation

Este repositório destina-se à realização dos testes de avaliação do SDK de integração de dispositivos do HDash.  

O [HDash](http://hdash.com.br/) (formalmente denominado HealthDash) é uma plataforma de monitoramento remoto de pacientes (RPM), em desenvolvimento no [Laboratório de Pesquisas em Redes e Multimídia (LPRM)](https://lprm.inf.ufes.br/) da [Universidade Federal do Espírito Santo (UFES)](http://ufes.br/).

O objetivo do SDK é fornecer interfaces para a criação de *drivers* de comunicação com diferentes dispositivos de saúde, facilitando sua integração com a plataforma.

O SDK foi construído utilizando TypeScript, permitindo sua execução em diferentes arquiteturas de hardware e sistemas operacionais. Desta forma, ao escrever um *driver*, o desenvolvedor não precisa se preocupar com os diferentes cenários de execução, que serão tratados pelo HDash.

Simuladores capazes de reproduzir diferentes tipos de dispositivos foram criados para esta avaliação. Assim, para realizá-la não será necessário ter os dispositivos de saúdes, uma vez que o simulador irá imitar o seu comportamento.

Nesta avaliação são propostos três cenários: o primeiro utilizando um *driver* já existente, o segundo criando um novo *driver*, mas reaproveitando o protocolo de comunicação e o último cenário, em que um *driver* completo será criado, incluindo o protocolo de comunicação.

## Recursos necessários

Para executar esta avaliação, você precisará de um computador com navegador Chrome e conexão com a Internet. Também será necessária uma conta no [Gitpod](https://gitpod.io/), que será utilizado como editor para desenvolvimento dos *drivers* e como ambiente para execução da simulação.

O tempo estimado para finalização da avaliação é de cerca de 2 horas. Ao final da avaliação, você será convidado a responder um questionário, disponível no link: [forms.google.com/xxxxx.](forms.google.com/xxxxx.).

Para iniciar a avaliação, clique no botão abaixo. Ele te redirecionará ao Gitpod, preparando o ambiente necessário para o desenvolvimento dos testes. Você pode manter uma aba aberta para desenvolver os cenários descritos abaixo, ou abri-lo novamente no editor do Gitpod.

[![Abrir no Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/hdash/hdash-sdk-evaluation)

Cada cenário de avaliação possui seu próprio diretório neste repositório. Os testes serão executados utilizando [NodeJS](https://nodejs.org/en/). Para facilitar esta tarefa, optamos pela criação de um monorepo utilizando [Workspaces do NPM](https://docs.npmjs.com/cli/v8/using-npm/workspaces). Os comandos para compilação e execução do código serão detallhados nas seções abaixo.

## Cenários de avaliação

Imagine que você foi contratado por uma empresa que deseja iniciar um estudo piloto para monitoramento remoto de pacientes crônicos. Neste estudo de caso, serão acompanhados indivíduos diabéticos e hipertensos, e serão utilizados três diferentes dispositivos: dois tipos de glicosímetros (de fornecedores distintos) e um medidor de pressão arterial.

O primeiro glicosímetro comunica-se por Bluetooth Low Energy e segue a especificação GATT (*Generic Attribute Profile*) para dispositivos desta natureza. Como o HDash SDK já possui suporte aos perfis GATT, será necessário apenas criar o descritor para o dispositivo.

O segundo glicosímetro, comunica-se por Bluetooth Serial. Ele segue o mesmo protocolo de comunicação do primeiro dispositivo, mas sobre outro protocolo de conectividade. Neste caso, você poderá reaproveitar comandos e respostas já existentes no SDK, mas terá que orquestrá-los para que a comunicação com o dispositivo funcione.

O medidor de pressão arterial comunica-se via USB e utiliza um protocolo de comunicação proprietário, que ainda não está disponível no HDash SDK. O fornecedor do dispositivo disponibilizou a documentação que detalha o protocolo de comunicação utilizado e você deverá implementá-lo. 

### Cenário 1 - Utilizando um driver já existente

No primeiro cenário, como já existe um driver pronto no HDash, será necessário implementar apenas o descritor do dipositivo. Isto deverá ser feito no arquivo ```descriptor.ts```.

### Cenário 2 - Reaproveitando um protocolo de comunicação

### Cenário 3 - Criando um driver completo